# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/fluxcd/flux" {
  version     = "1.1.2"
  constraints = ">= 1.0.0"
  hashes = [
    "h1:9MrcfWkTBE0ir7FuCWdM2ryC7fk3A/WwgghwANjRpq8=",
    "zh:03adb50b39c3b10b151b1f999cc18b51b0c42bebda342b140f047aa1604573ae",
    "zh:0b0b236d18549c25288570018b2df91a12302861365a90bb414a6a38f226423f",
    "zh:0c49dd70ef6c29fa0aff749bee51a426e5c800ca3a42158acaca3a7b80c4a776",
    "zh:303b217eebcdda87e0959c7287e53f8b07c3c0e78568227b63c0f083fd7b6caf",
    "zh:771e20a83ee6c5bd910704d9ead7113b2b7557cb75b129d0245de8b5a3edf055",
    "zh:81db07ac4b71f4447b47dc63830626f5aef45c8d0bd34536f4f46c7d55990206",
    "zh:8397848b483fe3e75f3761577b0721e1921c1bc802860c4171360de3f264f5e9",
    "zh:9097f36f8a7ad6bd2ea6c0a001c32f1b1cb2c5d3ac133b688370dcf6f47ad435",
    "zh:9ba8de8f918f9af558817b80c616bf55c2814e20ea47a71a583fd421fb402935",
    "zh:9fb55e50256fc538b17bb9d17dc59fb08cc9b04f3e9916948381a1fb29f8d4de",
    "zh:ced0ac7486223785532651a4c639f849f84e371cb552d06802cc1d5f78ee8929",
    "zh:dd9952f6ec1e8144b12fb3e137e46d9cbe14cad8ae3ef14908e8ac146d7b912a",
    "zh:e297e5276cd0533a298fe65b791b40b3fc0110bdd052346dd0cc8802e51b00e9",
    "zh:f6fd2295c7502af0697e7a82c5a71c4959be65811c0ad73f80c0c1ca8903f05b",
  ]
}

provider "registry.terraform.io/tehcyx/kind" {
  version     = "0.2.0"
  constraints = "0.2.0"
  hashes = [
    "h1:vJ3Ge3x4KZRVzZHcW8kBolUQQEdEKLbm2zw5k5fhPUs=",
    "zh:21f5b71227106d04fc18d2d545bf688ae33efd160bb96f2aa894ae7859afc2e3",
    "zh:28daa5fd6b9d4f797e6aa36a2e14f9d53a9ac485c932bceca9c8d078c1957d7d",
    "zh:2c1e4cce4402916aa6948a77f23088b525e7c8222113b84f679b95f1c2b353f7",
    "zh:32e95548ea7b9b6d5aba4231b75e6d955fbae3aa288c39935aa4ff224936dd88",
    "zh:3c4c0a32f3e111bfdfca4c950d4dad8a5a568a471ea702358848f61fe812d409",
    "zh:ee375582c2b92e466b2397a9cacef463f21e043616773de5c66021c36b3fd172",
  ]
}
